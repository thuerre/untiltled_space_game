﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour 
{
    public Transform firePoint;
    public GameObject bulletPrefab;
    public float bulletForce = 20f;
    private float NextFire;
    public float FireRate  = 0.2f;
    public Vector2 direction;

    // Update is called once per frame
    void Update()
    {
      
        if (Input.GetAxisRaw("Shoot1")>0 && Time.time > NextFire)
        {
            Shoot();
            NextFire = Time.time + FireRate;

        }
    }
    void Shoot()
    {
        //Vector2 mousePos = Input.mousePosition;
        //mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        Vector2 direction = new Vector2(
            Input.GetAxisRaw("AimHorizontal1") /*- transform.position.x*/,
            -Input.GetAxisRaw("AimVertical1") /*- transform.position.y*/);
            //mousePos.x - transform.position.x,
            //mousePos.y - transform.position.y);
        firePoint.up = direction;
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.up * bulletForce, ForceMode2D.Impulse);
    }
}

