﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class TIH_ControllerMovement : MonoBehaviour
{

    PlayerControls controls;
    Vector2 Move;

    void Awake()
    {
        controls = new PlayerControls();

        controls.Gameplay.Move.performed += ctx => Move = ctx.ReadValue<Vector2>();
        controls.Gameplay.Move.canceled += ctx => Move = Vector2.zero;
    }

    void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    void OnDisable()
    {
        controls.Gameplay.Disable();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 m = new Vector2(Move.x, Move.y) * Time.deltaTime;
        transform.Translate(m, Space.World);
    }
}
