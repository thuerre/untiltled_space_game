﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOnScreen : MonoBehaviour
{
   
    public Transform Player1Pos;
    public Transform Player2Pos;
    

    public Vector2 player1;
    public Vector2 player2;
    public Vector2 Verticalplayer1;
    public Vector2 Verticalplayer2;
    public float dist;
    public float Verticaldist;
    public Vector2 LastPos;
    

    // Update is called once per frame
    void FixedUpdate()
    {
        player1 = Player1Pos.position;
        player2 = Player2Pos.position;
        dist = Vector2.Distance(player1, player2);
        Verticaldist = (player1 - player2).magnitude;
        Vector2 currentPos = transform.position;
        
        if (GetComponent<Renderer>().isVisible != true )
        {
            transform.position = LastPos;
        }
        LastPos = currentPos;
        
    }
}
