﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee : MonoBehaviour
{


    public Transform firePoint;
    public GameObject slashPrefab;
    public float slashForce = 20f;
    public Vector2 ofset;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Melee1"))
        {
            Slash();
        }
        
    }

    void Slash()
    {
        Vector2 mousePos = Input.mousePosition;
        mousePos = Camera.main.ScreenToWorldPoint(mousePos);

        Vector2 direction = new Vector2(
            mousePos.x - transform.position.x,
            mousePos.y - transform.position.y);
        firePoint.up = direction;
        ofset.Set(transform.position.x + Mathf.Clamp(mousePos.x,-0.5f,0.5f),transform.position.y+ Mathf.Clamp(mousePos.y, -0.5f, 0.5f));
        GameObject bullet = Instantiate(slashPrefab, ofset , firePoint.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        
    }    
}

