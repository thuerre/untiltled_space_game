﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class slash : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject hitEffect;
    void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject effect = Instantiate(hitEffect, transform.position, Quaternion.identity);
        Destroy(effect, 0.1f);
        Destroy(gameObject);
    }
    private void OnEnable()
    {
        Destroy(gameObject, 0.1f);
    }
    void Start()
    {
        
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Destroy(gameObject, 0.1f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
