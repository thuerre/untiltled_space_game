﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LUD_Movement2 : MonoBehaviour
{
    // Start is called before the first frame update
    public float sprint = 10f;
    public float moveSpeed = 5f;
    public float BasemoveSpeed = 5f;
    public Rigidbody2D rb;
    Vector2 movement;
    public Animator animator;
    Vector2 mousePos;
    public Camera cam;
    public float direction;
    private bool isMoving;
    private float verticalDelta;
    private float horizontalDelta;

    // Update is called once per frame
    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal2");
        movement.y = -Input.GetAxisRaw("Vertical2");

        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        animator.SetFloat("Horizontal", movement.x);
        animator.SetFloat("Vertical", movement.y);
        animator.SetFloat("Speed", movement.sqrMagnitude);
        animator.SetFloat("Direction", direction);
        animator.SetBool("Moving", isMoving);

        if (movement != new Vector2(0, 0))
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        verticalDelta = Input.GetAxisRaw("AimHorizontal2");//- transform.position.x;
        horizontalDelta = -Input.GetAxisRaw("AimVertical2");// - transform.position.y;

        if (verticalDelta >= 0 && horizontalDelta >= 0)
        {
            if ((verticalDelta - horizontalDelta) >= 0)
            {
                direction = 0; //regarde en haut
            }
            else
            {
                direction = 1; //regarde à droite
            }
        }
        else if (verticalDelta >= 0 && horizontalDelta < 0)
        {
            horizontalDelta = -horizontalDelta;
            if ((verticalDelta - horizontalDelta) >= 0)
            {
                direction = 0;
            }
            else
            {
                direction = 3; //regarde à gauche
            }
        }


        if (verticalDelta < 0 && horizontalDelta >= 0)
        {
            verticalDelta = -verticalDelta;
            if ((verticalDelta - horizontalDelta) >= 0)
            {
                direction = 2; //regarde en bas
            }
            else
            {
                direction = 1;
            }
        }
        else if (verticalDelta < 0 && horizontalDelta < 0)
        {
            horizontalDelta = -horizontalDelta;
            verticalDelta = -verticalDelta;
            if ((verticalDelta - horizontalDelta) > 0)
            {
                direction = 2;
            }
            else
            {
                direction = 3;
            }
        }




        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift) || Input.GetAxisRaw("Sprint2") > 0)
        {
            moveSpeed = sprint;
        }
        else
        {
            moveSpeed = BasemoveSpeed;
        }


        // a voir lequel marche le mieux

        /*if(Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) 
        {
            moveSpeed = (moveSpeed+sprint);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift) || Input.GetKeyUp(KeyCode.RightShift)) 
        {
            moveSpeed = BasemoveSpeed;
        }
       */


    }

    void FixedUpdate()
    {
       
            rb.MovePosition(rb.position + movement.normalized * moveSpeed * Time.fixedDeltaTime);
        
        
        //edit Tim, essai de fix pour que les projectiles aillent là ou pointe la souris
        /*Vector2 lookDir = mousePos - rb.position;
          float angle = Mathf.Atan2(lookDir.y, lookDir.x) * Mathf.Rad2Deg - 90f;*/

    }
}
