﻿
using UnityEngine;

public class cameraMovement : MonoBehaviour
{
    public Transform target;
    public float cameraSmooth = 0.125f;
    
    private void FixedUpdate()
    {
        
        Vector2 smoothedPosition = Vector2.Lerp(transform.position, target.position, cameraSmooth);
        transform.position = smoothedPosition;
        transform.LookAt(target);

    }
}
